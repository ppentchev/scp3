use std::env;
use std::error::Error;
use std::io;
use std::io::Write;
use std::process;

use getopts::Options;

const USAGE_STR: &str = "Usage: scp3 src:/path/srcfile dst:/path

Note: the source file or directory will be copied *under the same name*
within the directory specified on the destination host.
The specified destination path must already exist as a directory on
the destination host!";

fn usage() -> !
{
	writeln!(io::stderr(), "{}", USAGE_STR).unwrap();
	process::exit(1);
}

fn split_hostspec(hostspec: &str) -> (&str, &str)
{
	let parts: Vec<&str> = hostspec.splitn(2, ':').collect();
	match parts.len() {
		2 => (parts[0], parts[1]),
		_ => usage(),
	}
}

fn split_pathspec(pathspec: &str) -> (&str, &str)
{
	let parts: Vec<&str> = pathspec.rsplitn(2, '/').collect();
	match parts.len() {
		1 => (".", pathspec),
		_ => (parts[1], parts[0]),
	}
}

fn main() -> Result<(), Box<dyn Error>>
{
	let args: Vec<String> = env::args().collect();

	let mut optargs = Options::new();
	optargs.optflag("v", "verbose",
			"verbose operation: display the files copied");
	optargs.optflag("N", "noop",
			"no-operation mode: display the files that would have been copied");
	let opts = optargs.parse(&args[1..]).expect(USAGE_STR);

	if opts.free.len() != 2 {
		usage();
	}

	let (srchost, srcpath) = split_hostspec(&opts.free[0]);
	let (dsthost, dstpath) = split_hostspec(&opts.free[1]);

	let (srcdir, srcname) = split_pathspec(srcpath);
	
	let mut src_ssh = process::Command::new("ssh")
		.args(&["--", srchost, "tar", "-cf", "-", "-C", srcdir, "--", srcname])
		.stdout(process::Stdio::piped())
		.spawn()
		.expect("Failed to spawn the SSH client to the source host");

	let src_ssh_out = src_ssh.stdout.take()
		.expect("Could not open the source SSH client's standard output stream");

	let tar_action = match opts.opt_present("N") {
		false => match opts.opt_present("v") {
			false => "-xpf",
			true => "-xvpf",
		},
		true => "-tvf",
	};
	let dst_ssh_result = process::Command::new("ssh")
		.args(&["--", dsthost, "tar", tar_action, "-", "-C", dstpath])
		.stdin(process::Stdio::from(src_ssh_out))
		.spawn()
		.expect("Failed to spawn the SSH client to the destination host")
		.wait()
		.expect("Failed to wait for the SSH client to the destination host to finish");

	let src_ssh_result = src_ssh.wait()
		.expect("Failed to wait for the SSH client to the source host to finish");

	if !src_ssh_result.success() {
		writeln!(io::stderr(), "The SSH client to the source host failed")?;
		process::exit(1);
	}
	if !dst_ssh_result.success() {
		writeln!(io::stderr(), "The SSH client to the destination host failed")?;
		process::exit(1);
	}

	Ok(())
}
